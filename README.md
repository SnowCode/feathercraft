# FeatherCraft
PaperMC configuration for very lightweight and performance-focused server. The optimizations are mainly coming from [this Spigot thread](https://www.spigotmc.org/threads/guide-server-optimization%E2%9A%A1.283181/) about server optimization. You can go on it to have more information about what each changes do.

## Features
* Optimization of tons of things in bukkit.yml, spigot.yml, paper.yml and server.properties
* Support all plugins
* Chunky plugin built-in

## Install
1. Clone this repository 

```bash
git clone https://codeberg.org/SnowCode/feathercraft
cd feathercraft
```

2. Get the paperMC jar into the feathercraft directory by downloading it from [paperMC download page](https://papermc.io/downloads)

3. Allocate RAM to the server (don't allocate ALL your ram or the server will crash!!)

```bash
# I.e allocate 4G of ram
sed -i 's/2G/4G/g' start.sh
```

4. Run the sevrer using the start script

```bash
bash start.sh
```

5. Setup the world border using those commands (to type in the console)

```
worldborder center 0 0
worldborder set <how you want your world to be>
chunky worldborder
chunky start
```

6. Join the server by connecting to the IP. To set yourself administrator of the server, type this in the console: 

```
op <your player name>
```

## Personal configuration
A configuration of course already been made on the project (it's the main purpose), but you may also want to configure your own things.

Here are some exmaple of things you may want to change in the configuration files or directories.

### server.properties

* `online-mode` change this one to false if you want to allow cracked (free launcher) players to join the game. 

### Changing the world
PaperMC doesn't use the same world management format as the classic launcher (instead of world/ it uses world/, world_nether/ and world_end/) but it's not a problem it can convert them.

To change the map do the following steps:

1. Either move the existing world*/ directories, or remove them.

```
mkdir old_world
mv world*/ old_world/
```

2. Then move the map you want into that directory (if it's a zip file, don't forget to extract it). Then rename it as "world/"

```
mv ~/.minecraft/saves/myWorld/ world/
```

3. Restart the server.

```
bash start.sh
```

4. Join the game, and go at the center of the map to run the following in-game commands:

```
/worldborder center ~ ~
/worldborder set <how large is your map, i.e 2000>
/chunky worldborder
/chunky start
```



